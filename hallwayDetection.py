import cv
import cv2
import numpy as np
import sys

# check which grid location a point falls in
def vanishpt(pointsx, pointsy, width, height, img):
	binwidth = width / 11.0
	binheight = height / 11.0
	hist, xedges, yedges = np.histogram2d(pointsx, pointsy, bins = 11, range = [[0,640],[0,480]])
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	hist = np.rot90(hist)
	hist = np.rot90(hist)
	hist = np.rot90(hist)
	#print hist
	#cv2.namedWindow('vanishing point', cv.CV_WINDOW_AUTOSIZE)
	#cv2.imshow('vanishing point', hist)
	vp = np.unravel_index(hist.argmax(), hist.shape)
	binY = vp[0]
	binX = vp[1]
#	binY = 0;
#	binX = 0;
	print width
	print height
	print vp
	pt1 = ((int)(binwidth*(binX+0)), (int)(binheight*(binY+0)))
	pt2 = ((int)(binwidth*(binX+1)), (int)(binheight*(binY+1)))
	#pt1 = ((int)(binheight*(vp[1]+1)), (int)(binwidth*(vp[0]+1)))
	#pt2 = ((int)(binheight*(vp[1]+0)), (int)(binwidth*(vp[0]+0)))
	print pt1
	print pt2
	cv2.rectangle(img, pt1, pt2, (0,255,0), 1)	
	return vp # tuple of vanishing point	
	


# find intersection of lines from rho-theta pairs
def findIntersect(r0,t0,r1,t1):
	a = np.cos(t0)
	b = np.sin(t0)
	c = np.cos(t1)
	d = np.sin(t1)
	det = a*d - b*c
	if(det != 0.0):
		x = (int)( (d*r0 - b*r1) / det)
		y = (int)( ( -c*r0 + a*r1) / det )
		return x,y
	else:
		return 0

def hallclass(img):
#	global img
	#img = cv2.imread(sys.argv[1])
	height, width, depth = img.shape

	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	edges = cv2.Canny(gray,100,200, apertureSize = 3)


	#cv2.namedWindow('Canny Edge', cv.CV_WINDOW_AUTOSIZE)
	#cv2.imshow('Canny Edge', edges)

	# remove vertical lines
	rmVert = np.zeros((15,3),dtype=np.uint8)
	rmVert[7,...] = 1 # create morphology objecti
	#print rmVert
	#edges2 = cv2.morphologyEx(edges, cv2.MORPH_OPEN, rmVert, iterations=1)
	edges2 = cv2.Sobel(edges, -1,1,1, ksize=3)
	#cv2.namedWindow('Remove Vert Lines', cv.CV_WINDOW_AUTOSIZE)
	#cv2.imshow('Remove Vert Lines', edges2)


	lines = cv2.HoughLines(edges2,1,np.pi/180,100)
	if lines.any():
		# hough line drawing
		for rho,theta in lines[0]:
			a = np.cos(theta)
			b = np.sin(theta)
			x0 = a*rho
			y0 = b*rho
			x1 = int(x0 + 1000*(-b))
			y1 = int(y0 + 1000*(a))
			x2 = int(x0 - 1000*(-b))
			y2 = int(y0 - 1000*(a))
			cv2.line(img,(x1,y1),(x2,y2),(0,0,255),1)

		pointsx = []
		pointsy = []
		# find line intersections
		for rho0,theta0 in lines[0]:
			for rho1,theta1 in lines[0]:
				loc = findIntersect(rho0, theta0, rho1, theta1)
				#print loc
				if(loc != 0):
					cv2.circle(img,loc,1,(255,0,0),1)
					pointx = loc[0]
					pointy = loc[1]
					pointsx.append(pointx)
					pointsy.append(pointy)

		vp = vanishpt(pointsx, pointsy, width, height, img)

		cv2.namedWindow('Hall Detection: Vanishing Point', cv.CV_WINDOW_AUTOSIZE)
		cv2.imshow('Hall Detection: Vanishing Point', img)


		#lines = None 
		#image2 = cv.CreateImage(cv.GetSize(image), 8, 1)
		#cv.CvtColor(image, image2, cv.CV_BGR2GRAY)
		#ihough = cv.HoughLines2(image2, lines, 1, 1, 1, 1)
		#cv.ShowImage('window1', image2)
		return len(lines[0]), vp
	else:
		return -1,(-1,-1)

#def main():
#	
#	img = cv2.imread(sys.argv[1])
#	print hallclass(img)
	#cv2.waitKey(10000)

#main()
