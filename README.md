#ARDrone-Nav

##Mark Williams | RIT CS Master's Project
[Master's Project Page](http://www.cs.rit.edu/~mxw9560/project/)
Sonar-Based Autonomous Navigation and Mapping of Indoor Environments Using Micro-Aerial Vehicles. 

Autonomous Navigation and Mapping using a Parrot AR.Drone Quadcopter with additional sonar sensors.
Running in a VirtualBox Ubuntu 12.04 VM. Communicate with Drone via wifi, with sensors via XBee.

Implementing a Hough Transform based landmark mapping system with EKF SLAM, detailed [here](http://groups.csail.mit.edu/marine/pub/Tardos02ijrr.pdf) 
Landmarks are classified as either a point or an infinite line.
Landmarks are stored as a Rho Theta pair, where Rho is the distance from the origin to the point,
and Theta is either the angle to the point, or the angle to the to the closest point on the line to the origin.

Mapping progress images [here](http://imgur.com/a/9obp2#0)

##Running with:

  * [self-modified libardrone](https://github.com/venthur/python-ardrone)
  * python 2.7
  * [python xbee](https://code.google.com/p/python-xbee/)
  * numpy
  * python opencv (cv2)
  * pygame 1.6
  * [scipy](http://www.scipy.org/install.html)

###Weird notes:
ardrone navdata communication does not seem to work with
VMWare. Switched back to VirtualBox
Sonar are stupid and way more inaccurate than predicted. Getting weird interference in different rooms.
And also having problems with the sonar angle varying due to drone pitch/roll/yaw.
