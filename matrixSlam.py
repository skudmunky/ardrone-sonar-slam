import numpy as np
from numpy import linalg as LA
from math import sin, cos, sqrt, tan
import random
import math
# simultaneous localization and mapping 
class matrixSlam:

    def __init__(self):
        self.motCov = [ [.05, 0, 0], [0, .05, 0], [0,0,.05] ]
        self.timeUd = None
        self.obsUd = None
        self.XhatBF0 = [ 0,0,0 ] # starts with perfect robot position, size is 3+2k
        self.PBF0 = np.zeros((3,3)) # size of pbf0 is 3+2k * 3+2k

        self.Fk    = None #transform previous coordinates to new coordinates
        self.Gk    = None  #transform motion to global map
        self.curPos = np.zeros((3), np.float)
        self.newPos = np.zeros((3), np.float)

        self.predictMapatk = np.zeros((3), dtype=np.float)
        self.predictCovatk = np.zeros((3,3), dtype=np.float)
        self.curSize = 3
        self.offsets = [ [-.250, 0, -90], [-.200, .250, -10], [.200, .250, 10], [.250, 0, 90] ]

        self.rb = 160
        self.tb = 180
        self.windowLength = 0 # set by send scalar
        self.numsensors = 0
        self.rbs = 320 / self.rb
        self.tbs = 360 / self.tb;
        self.Rkscale = 10

    def initBase(self, x,y,angle): # needed since the drone does not start mapping at 0,0,0
        self.curPos[0] = x
        self.curPos[1] = y
        self.curPos[2] = angle

    # sensorWindow = [sonarX, sonarY, sonarYaw, sensedRange, pose[0], pose[1], pose[3]] for each sensor k
    def run(self, sensorWindow, newPose, glandmarks, pointvotes, linevotes):
        self.motUpdate(newPose, glandmarks) # run the motion update
        zk = self.createZk(sensorWindow)    # create zk matrix
        hk, Hk, landmarkList = self.createHk(sensorWindow, glandmarks)
        #print(' zk --- hk ')
        #for i in range(0,len(zk)):
        #    print(' %.2f  |  %.2f') % (zk[i], hk[i]),
        #    if sensorWindow[i][3] == 0 and sensorWindow[i][0] != 0.0:
        #        print(' - point')
        #    elif sensorWindow[i][0] != 0.0:
        #        print(' - line')
        #    else:
        #        print(' - no feature')
       
        Kk = self.createKk(Hk, landmarkList, sensorWindow, pointvotes, linevotes)
        xhatBfk = self.predictMapatk + Kk.dot(zk - hk) 
        I = np.identity( (3+2*len(glandmarks)))
        pBfk = (I-Kk.dot(Hk)).dot(self.predictCovatk)

        self.predictCovatk = pBfk
        self.predictMapatk = xhatBfk
        print("New estimated robot position: %.4f, %.4f, %.4f") % (xhatBfk[0], xhatBfk[1], xhatBfk[2])
        estpos = xhatBfk[0:3]
        return estpos

    # send scalar values from sonarSLAM
    # so that the same values are used in each file
    # for rho bin and theta bin and associated scalars
    def sendScalars(self, rhobin, thetabin, wl, ns):
        self.rb = rhobin
        self.tb = thetabin
        self.rbs = 320 / self.rb
        self.tbs = 360 / self.tb
        self.windowLength = wl
        self.numsensors = ns

    def createZk(self, sensorWindow):
        h = len(sensorWindow)
        zk = np.zeros((h), dtype=np.float)
        for i in range(0,h):
            #print sensorWindow[i]
            zk[i] = sensorWindow[i][4][2][3] # copy sensor window range into zk
        return zk 
           
    def makeNoise(self,pose):
        pose = np.array(pose)
        scalar = 0.01 # scale dx,dy,d0 by a number to create noise proportional to the change in position
        noise = pose.dot(scalar) # scale sensor noise by the size of the difference
        for i in noise:
            i = i * (random.randrange(0,1)*2-1) # make randomly negative or positive
        #print noise    
        #return noise
        return pose

    # create the h sub k matrix of sonar positions associated with landmarks.
    # take in zk
    # find existing landmark associated with zki
    # using expected robot position (created by motion update), take the expected position
    #   and calculate the distance from the sonar to the existing landmark
    #
    # very computationally expensive
    def createHk(self, sensorWindow, glandmarks):
        # be VERY careful here, the number of sensors is HARDCODED MAGIC NUMBER and defines the size of Hk.
        Hk = np.zeros( (self.windowLength*self.numsensors, 3+2*len(glandmarks)), dtype=np.float) # 40 by 3*2k matrix
        h = len(sensorWindow)
        print h
        print len(glandmarks)
        #print h
        hk = np.zeros((h), dtype=np.float)

        landmarkList = []
        for i in range(0,h):
            X = None
            Y = None
            si = sensorWindow[i]
            #print si
            sensorInfoi = si[4]
            #print sensorInfoi
            rho         = int(si[0])
            theta       = int(si[1])
            votes       = si[2]
            pointorline = si[3]
            whichSensor = sensorInfoi[2][7] # left, front left, front right, or right sensor
            odometry = sensorInfoi[2][4:7]
            robPosi = np.array(odometry)
            robPossi = robPosi + self.makeNoise(robPosi) # add noise to the odometry robot position at step i 
            #print ('Which sensor? '), whichSensor
            #print robPossi
            # is all this neccessary or could I just take the range reading and add noise to it? This seems to be a bit overkill
            nsX = robPosi[0] + (self.offsets[whichSensor][0] * cos(robPosi[2])) - (self.offsets[whichSensor][1] * sin(robPosi[2]))
            nsY = robPosi[1] + (self.offsets[whichSensor][0] * sin(robPosi[2])) + (self.offsets[whichSensor][1] * cos(robPosi[2]))
            ns0 = math.degrees(robPosi[2]) + (self.offsets[whichSensor][2])
            sensedrange = 0.0

            rhos,thetas = (rho*  self.rbs * .1)-16, math.radians(theta * self.tbs) # do this math here instead of in each function
            sx, sy, s0 = nsX, nsY, math.radians(ns0) # do math here instead of in each function
            thetas = thetas + 0.001 # to account for float division by zero errors?
            index = 0
            for lm in range(0,len(glandmarks)): # in the landmark exists in global landmarks, calculate the expected distance
                if glandmarks[lm] == [rho,theta]:
                    #print('Landmark found in global list %d, %d') %(rho, theta) 
                    # now, take the new sensor location after noise modifications
                    # and calculate the distance from it to the landmark it voted for
                    if pointorline == 0:
                        #print('Point: calculating expected distance!')
                        sensedrange, X, Y =self.findrangetoPoint([rho,theta],[nsX,nsY,ns0])
                        Hk[i][0]        = self.drdx(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # partial derivative range verse x
                        Hk[i][1]        = self.drdy(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # range verse y
                        Hk[i][2]        = self.drdt(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # range verse 0
                        Hk[i][3+index]     = self.drdrho(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # range vs rho lm
                        Hk[i][3+index+1]   = self.drdtheta(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # range vs theta lm
                        landmarkList.append([rho,theta,0])
                    elif pointorline == 1:
                        #print('Line: calculating expected distance!')                
                        sensedrange, X, Y = self.findrangetoLine([rho,theta],[nsX,nsY,ns0]) 
                        #print('sensor: [%.2f, %.2f, %.2f] lm [%.2f %.2f] X %.2f Y %.2f range %.2f') % (sx,sy,s0,rhos,thetas,X,Y,sensedrange)
                        Hk[i][0]        = self.drdx(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # partial derivative range verse x
                        Hk[i][1]        = self.drdy(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # range verse y
                        Hk[i][2]        = self.drdt(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # range verse 0
                        Hk[i][3+index]     = self.drdrho(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # range vs rho lm
                        Hk[i][3+index+1]   = self.drdtheta(sensedrange, sx, sy, s0, rhos, thetas, X, Y) # range vs theta lm
                        landmarkList.append([rho,theta,1])
                    else: # landmark is not in global landmarks, take the
                        print('landmark not found.')
                        sensedrange = rho*self.rbs*.1 # take the sensed range as true since landmark is not in global landmarks 
                        landmarkList.append([rho,theta,2])
                index = index + 2 # increment index by 2 because of rho theta pairs in window
            hk[i] = sensedrange
            np.set_printoptions(precision=2)
            #print Hk[i]

        return hk, Hk, landmarkList

    # takes in the Hk matrix for multiplication and an in order list of landmarks so that
    # the votes associated with a landmark can be used to create the covariance of the landmark
    def createKk(self, Hk, landmarkList, sensorWindow, pointvotes, linevotes):
        print('Building Kk')
        #print sensorWindow
        plusminus = np.ones(self.numsensors) *  (random.randrange(0,1)*2-1)
        #Rk covariance needs to be a function of the number of votes a landmark received.
        Rk = np.identity(self.windowLength*self.numsensors) # covariance of the measurement noise - currently the identy matix times 6"
        for i in range(0,self.windowLength*self.numsensors):
            # figure out if its a point or a line
            rho = int(sensorWindow[i][0])
            theta = int(sensorWindow[i][1])
            #votes = sensorWindow[i][2]
            votes = 0 #sensorWindow[i][2]
            pol = sensorWindow[i][3] # p = 0 l = 1
            s = sensorWindow[i][4]
            sinfo = s[2]
            srange = sinfo[3]
            if pol == 0:
                #print('votes for %d %d = %d') % (rho, theta, pointvotes[rho,theta])
                #if pointvotes[rho,theta] > votes:
                votes = pointvotes[rho,theta]
            elif pol == 1:
                #print('votes for %d %d = %d') % (rho, theta, linevotes[rho,theta])
                #if linevotes[rho,theta] > votes:
                votes = linevotes[rho,theta]
            #print [rho,theta,votes,pol,pointvotes[rho,theta],linevotes[rho,theta]]
            scaleRange = votes / 255.0
            rkcovlm =  self.Rkscale*srange - self.Rkscale*(srange*scaleRange)
            
            Rk[i,i] = rkcovlm # HOW DO I CHOOSE THIS SCALAR
            print('The confidence of this landmark [%d,%d] with sensed range %.2f is %.2f with %d votes') % (rho, theta, srange, Rk[i,i],votes)
            if rho == 0:
                Rk[i,i] = 255

        Kk = self.predictCovatk.dot(Hk.T.dot( LA.matrix_power((Hk.dot(self.predictCovatk.dot(Hk.T))+Rk), -1) ))
        return Kk
        
        
    # partial derivative of range with respect to sensor x position
    def drdx(self, r, xs,ys,ts,rho,theta, X,Y):       
         
        return (1/(2*r))*(2*(xs-X)*(1-(tan(ts)/(tan(ts)+cos(theta)/sin(theta)))) + 2*(ys-Y)*(-(tan(ts)/(tan(ts)+cos(theta)/sin(theta))-tan(ts))))        

    def drdy(self, r,xs,ys,ts,rho,theta,X,Y):
        return (1/(2*r))*(2*(xs-X)*(-(-1/(tan(ts)+cos(theta)/sin(theta)))) + 2*((ys-Y)*(1-( -tan(ts)/(tan(ts)+cos(theta)/sin(theta)))+1)))

    def drdt(self, r,xs,ys,ts,rho,theta,X,Y):
        #print([r,xs,ys,ts,rho,theta,X,Y])
        sec2ts = 1+pow( tan(ts), 2)
        A = ( (xs*tan(ts) + rho/sin(theta) - ys)*(sec2ts) - (tan(ts)+cos(theta)/sin(theta))*(xs*sec2ts) )/ pow( (tan(ts) + cos(theta)/sin(theta)), 2  ) 
        B = ( tan(ts) * A + ( rho/sin(theta)-(ys-tan(ts)-xs) ) / ( tan(ts)+cos(theta)/sin(theta) ) - (xs*sec2ts) )
        #print('xs=%.2f, ys=%.2f, X=%.2f, Y=%.2f, A=%.2f, B=%.2f')%(xs,ys,X,Y,A,B)
        return (1/(2*r)) * ( 2*(xs - X) * (A) + 2*(ys-Y) * (B) )

    def drdrho(self,r, xs,ys,ts,rho,theta,X,Y):
        A = 1/sin(theta) * 1/(tan(ts) - cos(theta)/sin(theta))
        B = tan(ts)/sin(theta) * 1/(tan(ts)+cos(theta)/sin(theta))
        return (1/(2*r)) * ( 2*(xs-X) * (A) + 2*(ys-Y) * (B) )

    def drdtheta(self,r, xs,ys,ts,rho,theta,X,Y):
        csc20 = 1+pow(1/tan(theta),2) # csc^2 = 1+cot^2, cot = 1/tan
        A = ( rho/sin(theta) - ys + tan(ts)*xs* -csc20) / ( tan(ts) + 1/tan(theta)  ) 
        B = tan(ts) * A
        return (1/(2*r)) * ( 2*(xs-X) * (A) + 2 * (ys-Y) * (B) )

    # if a point was sensed by a sensor, we can just calculate the distance to the point
    # ignoring rho since we know the point has to be inside the sensor cone
    # need a way to bring scalars from sonarSLAM into this function
    def findrangetoPoint(self, point, robot):
        rho,theta = (point[0] *  self.rbs * .1)-16, math.radians(point[1] * self.tbs)
        rx, ry, r0 = robot[0], robot[1], math.radians(robot[2])
        #print('%.2f, %.2f | rx %.2f ry %.2f r0 %.2f') %(rho, theta, rx, ry, r0)
        if theta == 0:
            theta = theta + .01 # for div0 errors
        px = cos(theta)*rho
        py = sin(theta)*rho
        #print('px %.2f py %.2f') % (px,py)
        
        dx, dy = abs(rx-px), abs(ry-py)
        #print('dx %.2f dy %.2f') % (dx,dy)
        range1 = math.sqrt((dx*dx)+(dy*dy))
        return range1, px, py
            
 
    # finds the range from a sensor to a line feature
    # by finding the intersect of a line coming from the sensor at sensor angle
    # and the line feature        
    def findrangetoLine(self, line, robot):
        x1,y1,t1 = robot[0],robot[1],math.radians(robot[2]) # finding relation
        rho,theta = (line[0] *  self.rbs * .1)-16, math.radians(line[1] * self.tbs)
        
        # build a line coming out of the sensor
        slopes = math.tan(t1)
        yints = y1 - slopes*x1
        if theta == 0:
            theta = theta + 0.01 # for div 0 errors
        # now find the slope-intercept formula for our hough line
        slopeh = -cos(theta)/sin(theta)
        yinth = rho / sin(theta)
        #print('s: %.2f %.2f line %.2f %.2f') %(slopes,yints, slopeh ,yinth)

        # ys = slopes * x + yints
        # yh = slopeh * x + yinth
        # slopes * x + yints = slopeh * x + yinth
        # slopes * x - slopeh * x = yinth - yints
        # slopes-slopeh * x = yinth-yints
        # x = (yinth-yints)/(slopes-slopeh)
        x = (yinth-yints)/(slopes-slopeh)
        y  = slopes * x + yints
        dx, dy = abs(x1-x), abs(y1-y)
        #print('landmark theta = %.2f, line theta = %.2f') % (slopeh,slopes)
        range1 = math.sqrt((dx*dx)+(dy*dy))
        #print(' range1 '), range1
        return range1, x, y

    # new is the change in x,y,theta from previous
    # landmarks is the full list of sensed landmarks
    def motUpdate(self, new, landmarks):

        size = 3 + 2*len(landmarks) # 3+2k
        scalar = 0.001 # scale dx,dy,d0 by a number to create noise proportional to the change in position
        np.reshape(new, (1,3)).T # make vertical
        new = np.array(new) # turn new position into array for easier manipulation
        
        noise = new.dot(scalar) # scale sensor noise by the size of the difference
        for i in noise:
            i = i * (random.randrange(0,1)*2-1) # make randomly negative or positive
        #print noise 
        xold = self.curPos # get the previous position
        x1, y1, t1 = xold[0], xold[1], xold[2]
        x2, y2, t2 = new[0], new[1], new[2]
        self.curPos[0] = x1+x2+noise[0] # old + new + noise
        self.curPos[1] = y1+y2+noise[1] # old + new + noise
        self.curPos[2] = t1+t2+noise[2] # old + new + noise
        #print self.curPos
        self.predictMapatk = np.zeros((size)) # create map
        self.predictMapatk[0:3] = self.curPos[0:3] # copy x,y,0 into top of predicted map
        #print self.curPos
        
        expandLandmarks = [ x[i] for x in landmarks for i in range(0,2)] # turn 2*k array into 1*2k array
        self.predictMapatk[3:,] = expandLandmarks # copy sensed landmarks from window into predicted map
        #print self.predictMapatk

        # Fk and Gk matrixes are much simpler as we do not need the 2d transformations to transform
        # robots coordinate system to the maps coordinate system
        Fk = np.identity((size), dtype=np.float)
        Gk = np.zeros((size,3), dtype=np.float)
        # put identity matrix in the top 3x3 of Gk
        for i in range(0,3):
            for j in range(0,3):
                if i == j:
                    Gk[i][j] = 1 

        
        self.predictCovatk.resize((size,size)) # pad previous covariance matrix
        #print("Fk: "), np.shape(Fk)
        #print("Gk: "), np.shape(Gk)
        #print("Predict Cov: "), np.shape(self.predictCovatk)
        #print("Mot cov: "), np.shape(self.motCov)
        newCov = Fk.dot(self.predictCovatk).dot(Fk.T) + Gk.dot(self.motCov).dot(Gk.T)
        self.predictCovatk = newCov
        
    def printpfk(self):
        print('*** The new P F k|k-1 is:')
        print newCov
        print('*** The new x hat F k|k-1 is:')
        for i in range(0,len(self.predictMapatk)):
            if i == 0: 
                print('** pose **')
                print ('x: '),
            elif i == 1:
                print ('y: '),
            elif i == 2:
                print('0: '),
            elif i == 3:
                print('** Landmarks **')
                print('p: '),
            elif i % 2 == 1:
                print('p: '),
            else:
                print('0: '),
            print self.predictMapatk[i]
        print('*** Done with motion update')  

def main():
    slam = matrixSlam()
    sensedrange, sx, sy, s0, rhos, thetas, X, Y = 1.5, 1, 1, math.radians(45), 1.6, math.radians(45), 20, 20 
    print slam.drdt(sensedrange, sx, sy, s0, rhos, thetas, X, Y)
    #newpos = [1, 1, math.radians(45)]
    #landmarks = [ [1,3] ]
    #slam.motUpdate(newpos, landmarks)
    #newpos = [1,1,math.radians(45)]
    #landmarks = [ [1,3]]
    #slam.motUpdate(newpos, landmarks)
       
if __name__ == "__main__":
    main() 
