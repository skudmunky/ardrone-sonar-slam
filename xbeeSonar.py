# xbee-test.py
# reads serial data from an XBee version 2
# that is receiving information from an XBee connected
# connected to sonar sensors

from xbee import ZigBee
import numpy as np
import serial
import os
import time
import datetime
import csv

adcscale = (1.185/1023.0)   # adc vmax for range 0-1023
vscale = 2.78481012658      # convert 1.185 to the sensor output of 3.3 vmax
inch = 0.0064453125         # max range is 254 inches, 3.3 / 254 
#usbxbee = '/dev/ttyS0'     # serial port the USB Explorer is on
usbxbee = '/dev/ttyUSB0'
# dict structure for a single group of sensor readings
avg = []
timestamp = 0

# an array to hold the N most recent sensor reading objects
movingAvg = []
singleRead = []
sensorData = []
numReadings = 0

samples = 3
median = 2

# takes the latest sensor reading, adds it to the container
# and when needed calculates the median value of each sensor and returns it
def filter_data(data):
    global numReadings
    global movingAvg
    global timestamp
    global sensorData
    
    if numReadings < samples: # add new reading to container
        numReadings = numReadings + 1 
        movingAvg.append(data)
    if numReadings == samples: # average the readings 
        l = []
        fl = []
        fr = []
        r = []
        for row in movingAvg:
            #print row
            l.append(row[0])
            fl.append(row[1])
            fr.append(row[2])
            r.append(row[3])
        #TODO reject statistical outliers from each set of readings
                
        #lAvg = sum(l)/len(l)
        #flAvg = sum(fl)/len(fl)
        #frAvg = sum(fr)/len(fr)
        #rAvg = sum(r)/len(r)
        lAvg =  sorted(l)
        flAvg = sorted(fl)
        frAvg = sorted(fr)
        rAvg =  sorted(r)

        avg = [ lAvg[median], flAvg[median], frAvg[median], rAvg[median] ] # return the median of each set of readings
        timestamp = datetime.datetime.now().time() # time of the reading
        #print('Are you losing data from xbeeSonar?')
        if __name__ == "__main__": # for testing just this file
            print("Distance: [%.2f | %.2f | %.2f | %.2f ]") % (avg[0], avg[1], avg[2], avg[3])
            print( "Time: "),
            print timestamp
        movingAvg = [] # clear array
        numReadings = 0
        sensorData = avg # the global variable for a new sensor data

def read_sensor(running):
    #while True:
    try:
        print("* Opening serial connection to XBee USB Explorer at "),
        print usbxbee
        ser = None
        try:
            ser = serial.Serial(usbxbee, 9600, timeout=1) # 1 second timeout
            ser.timeout = 1
            xbee = ZigBee(ser)

        except Exception as e:
            print e
            
        #if os.environ.getenv("RUNNING") == "TRUE": # ENVIRONMENT VARIABLE
        #   print("Got environment variable RUNNING")
        #   running = True
        if ser != None:   
            with open('sonar.csv', 'wb') as csvfile:
                writer = csv.writer(csvfile, delimiter=',') 
                while running:
                    if __name__ != "__main__":
                        #print os.environ["RUNNING"]
                        if os.environ["RUNNING"] == "FALSE": # ENVIRONMENT VARIABLE
                            running = False
                    try:
                        response = xbee.wait_read_frame(timeout=1)
                        try:
                            adc = response["samples"]
                            la = adc[0]["adc-3"] # left
                            fla = adc[0]["adc-1"] # front left
                            fra = adc[0]["adc-2"] # front right
                            ra = adc[0]["adc-0"] # right
                            #print adc
                            FL = fla * adcscale * 2.66 / inch
                            FR = fra * adcscale * 2.66 / inch
                            L = la * adcscale * 2.66 / inch
                            R = ra * adcscale * 2.66 / inch
                                    
                            filter_data([L,FL,FR,R])
                            writer.writerow([L,FL,FR,R])

                        except Exception as e:
                            print("Failed response sample")
                            print response
                            print e
                    except Exception as e:
                        print("Warning! Timeout: Remote Xbee not sending data!")
                    except KeyboardInterrupt:
                        print("Keyboard interrupt: Xbee thread")
                        break
                print("Exiting serial connection to XBee")
                ser.close()
    except serial.SerialException, e:
        print("Could not open serial port %s: %s\n" % (ser.portstr, e))
        print("* Read timeout: Trying again!")
        ser.close()

def main():
    read_sensor(True)
if __name__ == "__main__":
    main()
