#autopilot.py
#control of an AR drone based on sensor info

hover = "HOVER"
forwards = "FORWARDS"
backwards = "BACKWARDS"
landing = "LANDING"
ground = "GROUND"

currentState = hover
nextState = hover

smin = 30
smax = 45
hovertime = 60
timer = 0

def autopilot(sensorData, autoEngage):
	global currentState
	global nextState	
	global timer

	action = hover
	FL = sensorData[1]
	FR = sensorData[2]
	#print("Autopilot Sensor Data: [%.2f | %.2f]") % (FL,FR)

	#if autoEngage == True:
	if currentState == hover:
		timer = timer + 1
		action = hover
		if timer == hovertime:
			timer = 0
			if FL > smax and FR > smax:
				nextState = forwards
				print("Switching to forward state")

	elif currentState == forwards:
		action = forwards
		if FL < smin or FR < smin:
			nextState = backwards
			print("Switching to backward state")

	elif currentState == backwards:
		action = backwards
		if FL > smax and FR > smax:
			nextState = hover
			print("Switching to hover state")

	elif currentState == landing:
		print("Handle landing?")
	else:
		print("unhandled state")
		action = hover
	#else:
	#	action = hover
	currentState = nextState
	return action


		
	
