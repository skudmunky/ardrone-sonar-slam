import cv
import cv2
import numpy as np
import sys

# takes in an image and runs stair detection on it
def stairclass(img):
	#global img
	#img = cv2.imread(sys.argv[1])
	width, height, depth = img.shape
	pixelsFromCeiling = int(height*.33) # max height of stair detection

	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	edges = cv2.Canny(gray,100,200, apertureSize = 3)

	#cv2.namedWindow('Canny Edge', cv.CV_WINDOW_AUTOSIZE)
	#cv2.imshow('Canny Edge', edges)

	# remove vertical lines
	rmVert = np.zeros((15,3),dtype=np.uint8)
	rmVert[7,...] = 1 # create morphology objecti
	#print rmVert
	edges2 = cv2.morphologyEx(edges, cv2.MORPH_OPEN, rmVert, iterations=1)
	edges2 = cv2.Sobel(edges, -1,1,1, ksize=3)
	#cv2.namedWindow('Remove Vert Lines', cv.CV_WINDOW_AUTOSIZE)
	#cv2.imshow('Remove Vert Lines', edges2)


	lines = cv2.HoughLinesP(edges,1,np.pi/180,100,minLineLength=60, maxLineGap=30)
	if len(lines) != 0: # if we have detected lines
		horzlines = []
		# hough line drawing
		# print lines
		for x1,y1,x2,y2 in lines[0]:
			slope = (np.abs(y1-y2)+.0000001) / (np.abs(x1-x2)+.0000001) # no div by zero here!
			if slope < .1:
				#if slope > 90:
				#	slope = 0
				#print slope
				horzlines.append( (x1,y1,x2,y2) )
				#cv2.line(img,(x1,y1),(x2,y2),(0,0,255),3)
		if len(horzlines) != 0:	
			# should probably check if horzlines contains shit	
			means = [np.mean(x) for x in zip(*horzlines)]
			#print means	
			# remove outliers
			stairlines = []
			#center=(int)(np.mean((means[0],means[2])))
			#cv2.line(img,(center,0),(center,480),(255,0,0),2)
			for x1,y1,x2,y2 in horzlines:
				#print x1, x2, means[0], means[2]
				if not(x1 > means[2] and  x2 < means[0]) and (y1 > pixelsFromCeiling and y2 > pixelsFromCeiling):
					stairlines.append((x1,y1,x2,y2))
					cv2.line(img,(x1,y1),(x2,y2),(0,0,255),3)

			means = [np.mean(x) for x in zip(*stairlines)]
			if means:
				center=(int)(np.mean((means[0],means[2])))
				cv2.line(img,(center,0),(center,height),(255,0,0),2)
				cv2.namedWindow('Staircase', cv2.CV_WINDOW_AUTOSIZE)
				cv2.imshow('Staircase', img)

				return len(horzlines), center
			else:
				return 0,-1
		else:
			return 0,-1
	else:
		return 0, -1

# main function for testing
def main():
	img = cv2.imread(sys.argv[1])
	print stairclass(img)
	cv2.waitKey(10000)

if __name__ == "__main__":
	main()
