# ar drone position estimate
# using vx vy vz theta phi psi from the ar drone navdata
# theta is pitch, phi is roll, psi is yaw. all in degrees
# vx vy vz are in mm/s 
# theta phi psi seem to update even if the drone is not flying
# vx vy vz do not update if drone is not flying

import time
import datetime
import math
import cv2
import numpy as np
import scipy
import cProfile
import cmath
import cairo
import csv
class posEst:
    def __init__(self):
        # position in meters
        self.posX = 0.0
        self.posY = 0.0
        self.posZ = 0.0
        # rotation in degrees
        self.pitch = 0
        self.roll = 0
        self.yaw = 0
        # time of last updating
        self.timestamp = time.time()
        
        # this always holds the odometry position
        self.rawX = 0.0
        self.rawY = 0.0
        self.rawYaw = 0.0

        # map
        # sensor offesets
        #           -X 
        #         v    v
        #       ( x /\ x )
        #  -Y   >)==||==(<   +Y
        #       ( x \/ x )
        #         
        #           +X
        # in meters. for testing purposes, 1 pixel will be 0.005 meters, giving a map resolution
        # of 2 meters in any direction on the 800x800 map
        self.mapwidth = 800
        self.mapheight = 800
        self.offsets = [ [-.250, 0, -90], [-.200, .250, -10], [.200, .250, 10], [.250, 0, 90] ] # left, front left, front right, right in meters
        #self.offsets = [ [-2.50, 0, -90], [-2.50, 0, -90], [2.50, 0, 90], [2.50, 0, 90] ] #for temp testing
        #self.offsets = [ [-.250, 0, -90], [-.250, 0, -90], [.250, 0, 90], [.250, 0, 90] ] # for supe rtemp testing
        self.localmap = np.zeros( (self.mapwidth,self.mapheight), np.uint8)
        self.localmap[:,0:self.mapwidth] = 128 # initialize entire map to having no obstacles
        self.mox = self.mapwidth/2 # map offset x
        self.moy = self.mapheight/2 # map offset y
        self.scaler = 40 # pixels per meter
        self.obsrange = 120 # range of an accepted obstacle in inches 120 inch ~= 3 meter
        self.obsprob = 5 # PIXELS on inside of obstacle distance for uncertainty

        # for making a drone overlay for the map
        self.overlay = np.zeros( (self.mapwidth,self.mapheight,3) , dtype=np.uint8)
        self.overlay[:,0:self.mapwidth] = [128,128,128]

        # localization declarations
        self.lineList = None # 2 dimensional array of hough lines
        self.pointList = None # 2 dimensional array of hough points
        self.window = None # sliding window update

        self.positionHistory = [[0.0,0.0,0.0]]
        self.sensorHistory = []

        self.iteration = 0
        self.windowlength = 10


        
    def localizationUpdate(self):
        recentPos = []
        recentSen = []
        for i in range(self.iteration-self.windowlength, self.iteration-1):
            #print i
            recentPos.append(self.positionHistory[i])
            recentSen.append(self.sensorHistory[i])
        #do hough transform stuff
    
    def updatePos(self, vx, vy, vz, theta, phi, psi, newTime):
        _vx = vx / 1000 #convert to meters/sec
        _vy = vy / 1000
        _vz = vz / 1000
        yaw = math.radians(psi)
        dt = newTime - self.timestamp

        self.posX = self.posX + dt * ( (_vx * np.cos(yaw)) - (_vy * np.sin(yaw)) )
        self.posY = self.posY + dt * ( (_vx * np.sin(yaw)) + (_vy * np.cos(yaw)) )
        self.posZ = self.posZ + dt * _vz
        self.yaw = yaw

        self.rawX = self.rawX + dt * ( (_vx * np.cos(yaw)) - (_vy * np.sin(yaw)) )
        self.rawY = self.rawY + dt * ( (_vx * np.sin(yaw)) + (_vy * np.cos(yaw)) )
        self.rawYaw = yaw
        self.timestamp = newTime
        self.logPos()

    def logPos(self): # log the poses to positionlog.csv
        with open('positionlog.csv', 'ab') as csvfile:
            x1,y1,z1,yaw1,ts = self.getPos()
            x2,y2,yaw2 = self.getRaw()
            writer = csv.writer(csvfile,delimiter=',')
            writer.writerow([x1,y1,yaw1,x2,y2,yaw2])
            
    def getPos(self):
        return self.posX, self.posY, self.posZ, self.yaw, self.timestamp

    def getRaw(self):
        return self.rawX, self.rawY, self.rawYaw

    def setPos(self, newpose): # this lets the EKFSLAM change the pose
        self.posX = newpose[0]
        self.posY = newpose[1]
        self.yaw = newpose[2]
        self.timestamp = time.time()

    def drawDronePose(self):
        for position in self.positionHistory:
            #print("---")

            px = position[0] 
            py = position[1]
            #print px
            #print py
            pyaw = position[2] 
            self.overlay[px,py] = [255,0,0]

        
    def updateMap(self, sensors):
        # left, front left, front right, right
        
        # convert sensor inch readings to meters 1 inch = 0.0254 meters
        sonar = [ x*0.0254 for x in sensors ] # scale to meters
        scaled = [ x*self.scaler for x in sonar] # scale meters to pixels 1 pixel = 0.01 meters
        
        #localization
        self.sensorHistory.append(sensors) # save sensors into sensor history 
        self.positionHistory.append( [ self.posX, self.posY, self.yaw ] )
        self.iteration = self.iteration + 1
        if (self.iteration % self.windowlength) == 0 and self.iteration > 0:
            self.localizationUpdate()


        # place the shape of the robot itself as obstacle free
        # AR.Drone is basically 517mm*517mm square 
        width = int(0.517*self.scaler)
        height = int(0.517*self.scaler)
        
        # build sonar probability arcs
        for i in range(0, 4):
            # relative positins of each sonar to the location and orientation of the drone
            sonX = self.posX + (self.offsets[i][1] * np.cos(self.yaw) - self.offsets[i][0] * np.sin(self.yaw))
            sonY = self.posY + (self.offsets[i][1] * np.sin(self.yaw) + self.offsets[i][0] * np.cos(self.yaw))
            sonYaw = math.degrees(self.yaw) + self.offsets[i][2]
            degrees = int(sonYaw)

            sonX = sonX * self.scaler + self.mox
            sonY = sonY * self.scaler + self.moy
            
            confidence = 0.5 # confidence of the sonar reading
           
            #print [sonX, sonY, sonYaw]
            #this is very slow and inefficient
            sonar = int(scaled[i])
            for u in range(degrees-15, degrees+15): # 30 degree uncertainty for the sonar
                confidence = 1.0 - (abs(u-degrees)*0.05) # scale confidence by degrees off sonar axis from .25 to 1
                if sonar < 80:   
                     for d in range (0, sonar): # changed to just draw obstacles, not entire sonar arc. This will speed drawing
                        pixel = int(255 * confidence)
                        
                        px = int(sonX + d * np.cos(u*0.017))
                        py = int(sonY + d * np.sin(u*0.017))
                        if px >=0 and py >= 0 and px <= self.mapwidth and py <= self.mapheight:
                            old = self.localmap[px,py]
                            new = old + abs(old-pixel)
                            if sensors[i] < self.obsrange: # obstacle detected if less than obsrange inches
                                if d == sonar-1: # actual obstacle reading
                                    # distance modifier on obstacle probability
                                    distmod = ( self.obsprob+1 - (sonar-d) + 0.001 ) / self.obsprob
                                    # new pixel is modified by the angular confidence and the distance confidence
                                    pixel = 0 + (255 - 255*confidence *(distmod) )
                                    new = old - abs(old-pixel)
                                    if new < 0: 
                                        new = 0
                                #elif d >= sonar-self.obsprob: # probabily leading towards obstacle
                                #    # distance modifier on obstacle probability
                                #    distmod = ( self.obsprob+1 - (sonar-d) + 0.001) / self.obsprob
                                #    # new pixel is modified by the angular confidence and the distance confidence
                                #    pixel = 0 + (255 - 255*confidence *(distmod) )
                                #    new = old + abs(old-pixel)
                                #    if new > 255: 
                                #        new = 255
                                else:
                                    new = 255 
                            else:
                                #new = old + abs(old-pixel)
                                #if new > 255:
                                #    new = 255
                                new = 255
                            self.localmap[px,py] = new  
                            self.overlay[px,py] = [new, new, new]             
        
        # put a black dot where the robot is
        px = self.posX * self.scaler + self.mox
        py = self.posY * self.scaler + self.moy
        #self.overlay[px,py] = [255,0,0]
        #self.overlay[px+1,py] = [255,0,0]
        #self.overlay[px-1,py] = [255,0,0]
        #self.overlay[px,py+1] = [255,0,0]
        #self.overlay[px,py-1] = [255,0,0]
        #print self.overlay.shape
             
    # quit - writes the localmap to a bmp file for later use 
    def quit(self):
        self.drawDronePose()
        datestring = datetime.datetime.now()
        dstring = datestring.strftime('%m%d%Y-%H%M%S')
        filename = "maps/Probability-Map-" + dstring + ".bmp"
        filename2 = "maps/Location-Map-" + dstring +".bmp"
        scipy.misc.imsave(filename2, self.overlay)
        scipy.misc.imsave(filename, self.localmap)

    # sliding window update for houghtransform landmarks
    # inputs are the [x,y,theta] drone position and [l,fl,fr,r] averaged sensor readings
    # N averaged sensor readings and drone positions make up the sliding window
    # and every Nth call the hough transform is run
    #def slidingWindow(self, pose, sensors):
       
        

# self test 
def main():
    dronepose = posEst()
    cv2.namedWindow('MAP', cv2.WINDOW_NORMAL)
    cv2.namedWindow('POSE', cv2.WINDOW_NORMAL)
    #cv2.resizeWindow('MAP', 800,800)
    angle = 0
    for i in range(1, 361):
        dronepose.updatePos(250.0, 0, 0, 0, 0, angle, time.time()) # 10 mm/s in x direction
        if i % 10 == 0:
            dronepose.updateMap([30, 36, 38, 35])
        if i % 2 == 0:
            angle = angle + 1 
        #print dronepose.getPos()
        cv2.imshow('MAP', dronepose.localmap)
        cv2.imshow('POSE', dronepose.overlay)
        cv2.waitKey(1)
        #time.sleep(0.066666666666667)
        #print dronepose.getPos()
        #print('Sending data')

    cv2.imshow('MAP', dronepose.localmap)
    cv2.waitKey(0)
    dronepose.quit()    
    #print dronepose.getPos()
    cv2.destroyAllWindows()


# test some position updates and see how long it takes to update the map
if __name__ == "__main__":
    cProfile.run('main()') 
