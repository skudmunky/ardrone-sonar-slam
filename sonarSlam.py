# sonar slam
# feature based SLAM using features detected
# via a sliding window of sonar sensor observations
# using a vote based system to select line and point features

import math
import numpy as np
import cv2
from collections import defaultdict as dd
import time
import posEst
import random
import matrixSlam

class sonarSlam:

    def __init__(self):
        self.windowLength = 20 # 10 sonar updates, 3 sonar updates per sonar reading    20 hz2222
        self.wi = 0 # window iterator
        self.numSensors = 4
        self.maxRange = 1 #meters
        self.prevPose = [0.0, 0.0, 0.0, 0.0] # x, y, z, yaw radians
        self.lineVotes = [ [0 for x in range(160)] for y in range(180) ]
        self.pointVotes = [ [0 for x in range(160)] for y in range(180) ]
        #self.slidingWindow = [ [[0.0 for x in range(7)] for y in range(self.numSensors)] for z in range(self.windowLength)]
        self.slidingWindow = [ [[0.0 for x in range(7)] for y in range(self.numSensors)] for z in range(self.windowLength)]
        #self.landmarks = [
        #print self.slidingWindow

        self.tb = 180 # number of theta bins (360 is 1 bin per 1 degree) 
        self.alphaInc = 360 / self.tb # need to make the alpha incrementer for sonar probability arc based off the theta bin resolution
        self.rb = 160 # number of rho bins  ( map size is 32 meters, 160 is .2 meters per bin)

        self.tbs = int(360 / self.tb) #scale 360 degrees to bins of theta
        self.rbs = int(320 / self.rb) #scale 32 meters to bins of rho
        self.rbos = 16 # rho bin offset for scaling -16/+16 to 0/32
        self.prevpose = [0.0,0.0,0.0,0.0] 
        self.rbi = 32.0 / self.rb # rho bin incrementer, for finding the closest rho bin for a given range
        
        self.hlinevotes =  np.zeros((self.rb,self.tb),np.uint8)
        self.hpointvotes = np.zeros((self.rb,self.tb),np.uint8)
        self.hspace =  [ [0 for x in range(4)] for y in range(self.windowLength*self.numSensors*30) ]
        self.hlinetable  = dd(lambda : dd(list))
        self.hpointtable = dd(lambda : dd(list))

        self.globalLineT = dd(lambda : dd (list))
        self.globalPointT = dd(lambda : dd (list))

        self.glandmarks = []    
        self.mapwidth = 800        
        self.mapscalar = 40 # 0.05 meters per pixel?
        self.mapoffset = 400
        self.linethresh = self.windowLength/3
        self.pointthresh = self.windowLength/2
        self.localmap = np.zeros((800,800,3))
        self.zk = []
        self.globalmap = []
        self.sensAssoc = None
        self.pointthreshwindow = 5
        self.linethreshwindow = 3
    # set point and line threshholds very low for quick self simulation
    def setthresh(self):
        self.pointthresh = 1
        self.linethresh = 1

    # continually adds sensor readings to a resetting sliding window
    # at end of window, calls the hough transform on the current window
    # and empties the window
    def addReading(self, pose, offsets,  sensors, ekf, posEst):
        # to add a reading, each sonar arcs position relative to the 0 point is calculated.
        # then, for 30 steps along the sonar arc, a line tangent to that point in the arc is calculated
        # this line is stored as a Rho Theta pair
        # Rho is the distance from the origin to the point on the line that is closest to the origin
        # Theta is the angle of Rho to the point or perpandicular to the line.


        # first, calculate position x,y,theta of each sonar
        #sonarLocs = [][]
        #print("Pose: %.2fx, %.2fy, @%.2f") %(pose[0], pose[1], pose[3])
        if self.wi < self.windowLength: # keep gathering data
            #print("Adding to update window: %d") % self.wi 
            for i in range(0,self.numSensors):
                sonarX = pose[0] + (offsets[i][0] * math.cos(pose[3])) - (offsets[i][1] * math.sin(pose[3]))
                sonarY = pose[1] + (offsets[i][0] * math.sin(pose[3])) + (offsets[i][1] * math.cos(pose[3]))
                sonarYaw = math.degrees(pose[3]) + (offsets[i][2]) # at this point, sonarYaw is in degrees
                #print ("%.2fx, %.2fy, @%.2f") %(sonarX, sonarY, sonarYaw)

                # need to store sensor range in meters, is currently in inches
                #if i == 0 or i == 3:
                sensedRange = sensors[i] * .0254 # .0254 meter per inch
                #else:
                #    sensedRange = 300*0.0254
                #print [self.wi, i]
                self.slidingWindow[self.wi][i] = [sonarX, sonarY, sonarYaw, sensedRange, pose[0], pose[1], pose[3], i] # save the sonar x,y,0, range, robot x,y,0
                #print("Sensor location %d: " ) % i,
                #print self.slidingWindow[self.wi][i]
            self.wi = self.wi + 1
        #if self.wi < self.windowLength-1: # keep gathering data
        #    #print("Adding to update window: %d") % self.wi 
        #    self.wi = self.wi + 1
        else: # at end of sliding window, run the hough transform
            self.wi = 0
            print ("Run the hough transform!")
            dx = abs(pose[0] - self.prevPose[0])
            dy = abs(pose[1] - self.prevPose[1])
            d0 = abs(pose[3] - self.prevPose[3])
            self.houghTrans(dx, dy, d0, pose, ekf, posEst) 
                
    # hough transform on the latest sliding window sensor update                  
    def houghTrans(self, dx, dy, d0, pose, ekf, posEst):
        
        newpose = pose
        self.hlinevvotes =  np.zeros((self.rb, self.tb), np.uint8)
        self.hpointvotes = np.zeros((self.rb,self.tb), np.uint8)
        self.hspace = np.zeros( ( (self.windowLength*self.numSensors*30), 7)  )
        #self.hspace =  [ [0 for x in range(7)] for y in range(self.windowLength*self.numSensors*30) ]
        #htablepoint =  [ [0 for x in range(160)] for y in range(180) ]
        #htableline =  [ [0 for x in range(160)] for y in range(180) ]
        self.zk = []
        theta = 0
        q = 0
        votes = 1
        self.sensAssoc = [[[] for q in range(0, self.windowLength)] for z in range(0, self.numSensors)] 
        # hough transform start
        lhp = np.zeros((self.rb, self.tb)) # local hough space for window update
        lhl = np.zeros((self.rb, self.tb)) # local hough space for window update
        self.hlinetable  = dd(lambda : dd(list)) # clear the local list of sensors associated with a line
        self.hpointtable = dd(lambda : dd(list)) # and with a point - does there need to be a similar association table for the global map|
        print("Starting hough transform")

        alphaSE = int(15 / self.alphaInc) * self.alphaInc # reduce alpha slightly so that the center alpha is always at 0
        #alphaSE = 15
        #self.alphaInc = 1
        # sensor = [sX sY angle range rX rY rAngle ]
        for i in range(0, self.windowLength):
            for j in range(0, self.numSensors):
                sX = self.slidingWindow[i][j][0] # sensor X (meters)
                sY = self.slidingWindow[i][j][1] # sensor Y (meters)
                sA = math.radians(self.slidingWindow[i][j][2]) # sensor Angle (radians)
                sR = self.slidingWindow[i][j][3] # sensed range (meters)
                rX = self.slidingWindow[i][j][4] # robot pose at this time
                rY = self.slidingWindow[i][j][5] # robot pose at this time
                r0 = self.slidingWindow[i][j][6] # robot pose at this time

                if sR < self.maxRange: # ignore sensor readings over a maximum range - speeds up calculations and minimizes sensor uncertainty
                    for a in range(-alphaSE, alphaSE, self.alphaInc): # sonar have a 30 degree uncertainty arc, increment the arc by the theta bin increment
                        alpha = math.radians(a)
                        theta = sA + alpha
                        
                        #line
                        if theta < 0:
                            theta = theta + math.radians(360)
                        thetaLineDeg = math.degrees(theta) # angle to the point on the line closest to 0,0
                        #point
                        roLine = sR + sX*math.cos(theta) + sY*math.sin(theta) # distance to point in line closest to 0,0
                        pX = sX + sR*math.cos(theta) # x location of point
                        pY = sY + sR*math.sin(theta) # y location of point
                        thetaPointDeg = math.degrees(math.atan2(pY,pX))
                        if thetaPointDeg < 0:
                            thetaPointDeg = thetaPointDeg + 360
                        roPoint = math.sqrt( (pX*pX) + (pY*pY) )

                        self.hspace[q] = [i, j, votes, roLine, thetaLineDeg, roPoint, thetaPointDeg]
                        q = q + 1

        #print("Discretizing hough space")
        # make the hough space data discrete for line and point votes
        for i in range(0, int((self.windowLength*self.numSensors*30))):
            
            ############################### line
            # bin for ro
            # ro is in -16 to 16 meters, shift to 0 to 32 meters
            # 0.2 meters per ro increment, 32 meters becomes 160 bins
            j = 0.0
            binRoLine = 0
            roOffsetLine = self.hspace[i][3] + 16.0
            if self.hspace[i][3] != 0.0:
                while( j < roOffsetLine ):
                    j = j + self.rbi 
                    binRoLine = binRoLine + 1
            if binRoLine >= self.rb:
                binRoLine = self.rb-1
            
            # bin for theta
            # 360 degrees in 2 degree increments, 180 bins
            binThetaLine = (int)(self.hspace[i][4] / self.tbs)
            if binThetaLine >= self.tb:
                binThetaLine = 0

            brl = int(binRoLine) #sometimes they arent ints?
            btl = int(binThetaLine)
                
            wi, si = int(self.hspace[i][0]), int(self.hspace[i][1])
            sensInfo = self.slidingWindow[wi][si]
            self.hlinetable[brl][btl].append([wi, si, sensInfo])

            lhl[brl][btl] = lhl[brl][btl] + self.hspace[i][2] # local hough line votes
            if lhl[brl][btl] > 255: lhl[brl][btl] = 255
            ############################

            ########################### point
            j1 = 0.0
            binRoPoint = 0
            roOffsetPoint = self.hspace[i][5] + 16
            if self.hspace[i][5] != 0.0:
                while j1 < roOffsetPoint:
                    j1 = j1 + self.rbi
                    binRoPoint = binRoPoint + 1
            if binRoPoint >= self.rb:
                binRoPoint = self.rb-1
            binThetaPoint = self.hspace[i][6] / self.tbs       
            if binThetaPoint >= self.tb:
                binThetaPoint = 0
            brp = int(binRoPoint)
            btp = int(binThetaPoint)

            self.hpointtable[brp][btp].append([wi, si, sensInfo])
            ###########################

            lhp[brp][btp] = lhp[brp][btp] + self.hspace[i][2] # local hough point votes
            if lhl[brp][btp] > 255: lhl[brp][btp] = 255

            self.prevPose = pose # at end of transform, update robot pose

        votedLandmarks = []
        for g in range(0,self.rb):
            for h in range(0,self.tb):
                if lhp[g][h] > self.pointthreshwindow and lhp[g][h] > lhl[g][h]: # if point vote > THRESH and > line vote
                    votedLandmarks.append([g,h, lhp[g][h],0])
                elif lhl[g][h] > self.linethreshwindow :#and lhl[g][h] >= lhp[g][h]:
                    votedLandmarks.append([g,h, lhl[g][h],1]) # elif line vote > THRESH and > point vote
        vl = np.array(votedLandmarks)
        
         
        if len(vl) != 0: # make sure landmarks exist
            vl = vl[vl[:,2].argsort()] # sort landmarks by number of votes received
            vl = vl[::-1] # reverse the array to sort in descending order of votes
        # [ rho, theta, votes, pointorline [ window step, sensor # [ info ] ] ] 
        # sensor = [sX sY angle range rX rY rAngle ]
        sensorAssoc = [[[0,0,0,0,[0,0,[0,0,0,0,0,0,0,0]]] for x in range(0,self.numSensors)] for y in range(0,self.windowLength)]
        for lm in vl: # for each landmark in voted landmarks
            #print lm # this displays the landmark for fun
            if lm[3] == 0: # point
                rho, theta, votes, pol = lm # get the information from the landmark
                sensors = self.hpointtable[rho][theta] # get the list of sensors in the current window associated with this landmark
                for s in sensors: # for each landmark associated with this point
                    si, sj = s[0], s[1] # sensor i and j ( i is window iteration, j is sensor0,1,2,3
                    if sensorAssoc[si][sj][2] ==0: # since we are going highest voted first, only add sensor info if sensor is unnassociated
                        sensorAssoc[si][sj] = [rho, theta, votes, pol, s] # associate this sensor with the landmark, along with all the relevant sensor info
                        self.hpointvotes[rho][theta] = self.hpointvotes[rho][theta] + 1 # increment the votes in the global landmark table
            elif lm[3] ==1: #lines
                rho, theta, votes, pol = lm # get the information from the landmark
                sensors = self.hlinetable[rho][theta] # get the list of sensors in the current window associated with this landmark
                for s in sensors: # for each sensor associated with this line
                    si, sj = s[0], s[1] # sensor i and j ( i is window iteration, j is sensor0,1,2,3
                    if sensorAssoc[si][sj][2] ==0: # since we are going highest voted first, only add sensor info if sensor is unnassociated
                        sensorAssoc[si][sj] = [rho, theta, votes, pol, s] # associate this sensor with the landmark, along with all relevant sensor info
                        self.hlinevotes[rho][theta] = self.hlinevotes[rho][theta] + 1 # increment the votes in the global landmark table

        sensorAssocList = []
        for x in range(0,self.numSensors):
            for y in range(0,self.windowLength):
                 sensorAssocList.append(sensorAssoc[y][x])
        #for sa in sensorAssocList:
        #    print sa
         
        print("Done with transform")
        
        #sort global landmarks into a list
        globalMap = []
        for i in range(0,self.rb):
            for j in range(0, self.tb):
                point = self.hpointvotes[i][j]
                line = self.hlinevotes[i][j]
                if line >= point and line > 0:
                    globalMap.append([i,j])
                elif point > line and point > 0:
                    globalMap.append([i,j])

        ### call slam stuff here ###

        ekf.sendScalars(self.rb, self.tb, self.windowLength, self.numSensors)# get the ekf portion on the same bin scaling as posest
        self.estpost = ekf.run(sensorAssocList, [dx,dy,d0], globalMap, self.hpointvotes, self.hlinevotes)
        print('### dead reckoning pose: ')
        print('x: '), pose[0]
        print('y: '), pose[1]
        print('0: '), pose[3]
        print('### finished with this window ')
        self.drawLandmarks()
        posEst.setPos(self.estpost) # set the robots pose to the EKF SLAM estimated pose

    # draw current list of point / line landmarks
    def drawLandmarks(self):
        for i in range(1,self.rb): 
            for j in range(0,self.tb): 
                l = self.hlinevotes[i][j] # votes for a line landmark at this ro theta pair
                p = self.hpointvotes[i][j] # votes for a point landmark at this ro theta pair
                

                if l > self.linethresh: # votes above line vote threshhold
                    #print self.hlinetable[i][j]
                    line = self.findLine([i,j]) # find the pixel coordinates to represent the infinite line
                    cv2.line(self.localmap, (int(line[0][0]), int(line[0][1])), (int(line[1][0]),int(line[1][1])),(0,0,255),2) # draw the line
                    cv2.circle(self.localmap, (int(line[2][0]), int(line[2][1])),1,(0,255,0),3) # draw the closest point on the line to the origin 0,0
                
                if p > self.pointthresh: # votes above point vote threshhold
                    point = self.findPoint([i,j]) # find the pixel coordinateds to represent the point
                    cv2.circle(self.localmap, (int(point[0]), int(point[1])), 1, (255,128,0), 5) # draw the point

        cv2.namedWindow('Hough landmarks', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('Hough landmarks', self.localmap)
        cv2.waitKey(1000)
        #self.displayHoughSpace()
                    
     
                
    # finds x and y points on the map to represent line landmarks
    def findLine(self,acceptLine):
        ro = acceptLine[0] * self.rbs * .1 # convert ro bin to 32 meters 
        theta = math.radians(((acceptLine[1]) * self.tbs))  # scale theta back to 360 degrees

        if theta == 0:
            theta = theta + math.radians(90.01)
        slope = (-math.cos(theta))/(math.sin(theta)) # does theta need to be radians?
        distance = (ro - self.rbos) # scale ro from 0 - 32 to -16 - 16
        intercept = (distance / math.sin(theta)) * self.mapscalar
        offset = self.mapwidth/2
        x1 = int(-(self.mapwidth/2))
        x2 = int(self.mapwidth/2)
        y1 = int(slope*x1+intercept)
        y2 = int(slope*x2+intercept)
        points = np.zeros((4,2), dtype=np.int)

        r = [ [0, -1], [1, 0] ] # 90 degree rotation matrix
        pts = [ [x1,y1 ], [x2,y2 ] ]
        rotated = np.dot(pts, r)
        
        points[0][0] = int((rotated[0][0]+offset))
        points[0][1] = ((offset-rotated[0][1]))
        points[1][0] = int((rotated[1][0]+offset))
        points[1][1] = ((offset-rotated[1][1]))
        points[2][0] = int( (math.cos(theta-math.radians(90))*distance)*self.mapscalar + self.mapwidth/2) # theta change is for visual representation
        points[2][1] = self.mapwidth - ( int((math.sin(theta-math.radians(90))*distance)*self.mapscalar + self.mapwidth/2))
        points[3] = acceptLine 

        return points
    
    # finds the x and y coordinates on the map to represent point landmarks
    def findPoint(self,acceptPoint):
        #print acceptPoint
        ro = acceptPoint[0] * self.rbs * .1 # convert to 0-32 meters
        theta = math.radians((acceptPoint[1]* self.tbs))#b ack to 360 degrees
        if theta == 0:
            theta = theta + 0.01 # div0
        distance = (ro  - self.rbos)
        pointX = int( ((math.cos(theta-math.radians(90))*distance)*self.mapscalar) + self.mapwidth/2) # theta change is for visual representation
        pointY = int( self.mapwidth-( int( (math.sin(theta-math.radians(90))*distance)*self.mapscalar) + self.mapwidth/2))
        return [pointX,pointY]
        
        
       
    def displayHoughSpace(self):
        
        cv2.namedWindow('line', cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow('point', cv2.WINDOW_AUTOSIZE)
        #line = 1 / np.array(self.hline)
        #point = 1 / np.array(self.hpoint
        #print self.hlinevotes
        #print line

        cv2.imshow('line', 255-self.hlinevotes)
        cv2.imshow('point', 255-self.hpointvotes)
        cv2.waitKey(1000)

    #def displayHoughLines(self, occgrid):
        
# performs a basic test of the hough transform based landmark mapping
# uses posEst.py  
def main():
    # test functionality?
    pose = posEst.posEst()
    slam = sonarSlam()
    ekf = matrixSlam.matrixSlam()
    slam.setthresh()
    random.seed()

    cv2.namedWindow('MAP', cv2.WINDOW_AUTOSIZE)
    
    offsets = [ [-.250, 0, -90], [-.200, .250, -10], [.200, .250, 10], [.250, 0, 90] ]
    sensors = [60.0, 255.0, 255.0, 60.0]
    angle = 17.3
    #simpleTest(pose, slam, angle, sensors, offsets, ekf)
    complicatedTest(pose, slam, angle, sensors, offsets, ekf)

def simpleTest(pose, slam, angle, sensors, offsets, ekf):
    for i in range(0, 300):
        pose.updatePos(220.0, 0, 0, 0, 0, angle, time.time()) # 10 mm/s in x direction 
        p = pose.getPos()
        if i % 10 == 0:
            slam.addReading( [p[0], p[1], p[2], p[3]], offsets, sensors, ekf, pose)
            pose.updateMap(sensors)
        cv2.imshow('MAP', pose.localmap)
        cv2.waitKey(1)
        time.sleep(.033)
    cv2.imshow('MAP', pose.localmap)
    cv2.waitKey(0)

def complicatedTest(pose, slam, angle, sensors, offsets, ekf):
    for i in range(0,501):
        pose.updatePos(320.0, 0, 0, 0, 0, angle, time.time()) # 10 mm/s in x direction 
        p = pose.getPos()
        if i == 140:
            angle = 75
        if i % 10 == 0:
            slam.addReading( [p[0], p[1], p[2], p[3]], offsets, sensors, ekf, pose)
            sensors[0] = 30 + random.randrange(-4,6,3)%6
            sensors[3] = 30 + random.randrange(-6,7,3)%6
            pose.updateMap(sensors)
        cv2.imshow('MAP', pose.localmap)
        
        cv2.waitKey(1)
        time.sleep(.05)
    cv2.imshow('MAP', pose.localmap)
    cv2.waitKey(0)
    #slam.displayHoughSpace() 
        
    

if __name__ == "__main__":
    main()
