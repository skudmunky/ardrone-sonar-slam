#test of hallwayDetector and stairwayDetector

import cv2
import sys
import numpy as np
import stairwayDetection
import hallwayDetection
from hallwayDetection import hallclass
from stairwayDetection import stairclass

def main():
    image = cv2.imread(sys.argv[1])
    img2 = cv2.imread(sys.argv[1])
    votesHall, grid = hallclass(image)
    votesStair, vanishpt = stairclass(img2)
    print "Stairway: {0}  || Hallway: {1}".format(votesStair, votesHall)
    if votesHall > 10 and votesHall > votesStair:
        print ("Hallway with {0} votes at grid {1}".format(votesHall, grid))
    elif votesStair > 10 and votesStair > votesHall:
        print "Stairway with {0} votes at horizontal pixel {1}".format(votesStair, vanishpt)
    else:
        print "Unknown"
    cv2.waitKey(0)

main()
