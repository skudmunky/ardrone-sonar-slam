#/usr/bin/python2.6
# wrapper for navigation and mapping using an ardrone

from threading import Thread
#import thread
from time import sleep
import xbeeSonar
import datetime
#import dronecontrol
import autopilot
import pygame
import libardrone
import pygame.surfarray
import pygame.font
import pygame.transform
import os
import math
import sonarSlam
import matrixSlam
import posEst

oldtime = 0
autoEngage = False
running = True

W, H = 1280, 720
dronestate = [0.0, 0.0, 0.0] # x, y, theta in inches
vx = 0.0
vy = 0.0
vz = 0.0
theta = 0.0
xbs = [0.0, 0.0, 0.0, 0.0] # xbee sonar readings

# autopilot and navigation control flags
goalReached = False
reached = False
enableMap = False
wander = False

STATE = "HOVER"
NEXTSTATE = "HOVER"

def initDrone():
    pygame.init()
    posest = posEst.posEst() # create posEst here so it can be used by the EKF slam thread
    drone = libardrone.ARDrone(posest)
    #drone = libardrone.ARDrone(is_ar_drone_2 = True, hd=True)
    drone.speed = 0.3 # lets make the drone slow
    clock = pygame.time.Clock()
    global running
    running = True
#   W, H = 320, 240
    screen = pygame.display.set_mode((W,H))
    #drone.reset()
    fpath = pygame.font.match_font('tlwgtypewriter')
    font = pygame.font.Font(fpath, 20)
    #drone.navdemoTrue()
    return drone, clock, screen, font, posest

# Gets all data from the drone synchronized with the xbee sensor refresh rate of 2 hz
def printSensor(drone, slam):
    global oldtime, vx, vy, vz, theta
    global xbs, enableMap
    offsets = [ [-.250, 0, -90], [-.200, .250, -10], [.200, .250, 10], [.250, 0, 90] ]
    while running:
        if xbeeSonar.timestamp != oldtime:
            xbs = xbeeSonar.sensorData
            if enableMap == True:
                drone.updateMap(xbs)
                #dronePose = drone.getPose()
                #pose = [ dronePose[0], dronePose[1], dronePose[2], dronePose[3] ]
                #slam.addReading(pose, offsets, xbs)
            #print xbs
            #navdata = drone.navdata
            #   print navdata
            #vx = navdata[0]['vx']
            #vy = navdata[0]['vy']
            #vz = navdata[0]['vz']
            #theta = navdata[0]['theta']
                
            #bat = navdata[0]['battery']
            #videoStatus = navdata['drone_state']['video_mask']
                
            #print("Battery: %.2f ") % bat,
            #print("|| [vx: %.2f vy: %.2f vz: %.2f theta: %.2f]") % (vx, vy, vz, theta),
            #print("Battery: %.2f ") % bat,
            #print("|| [vx: %.2f vy: %.2f vz: %.2f theta: %.2f]") % (vx, vy, vz, theta),
            #print(" || Sensor info: [ %.2f | %.2f ]") % (xbeeSonar.sensorData[1], xbeeSonar.sensorData[2])
            oldtime = xbeeSonar.timestamp


# change running environment variable for control of threads running in other files
def changeRUNNING(isrunning):
    if isrunning == True:
        os.environ["RUNNING"] = "TRUE"
    else:
        os.environ["RUNNING"] = "FALSE"

# drone keyboard control
# wasd and arrow keys control drone
# land = space
# take off = enter
# EMERGENCY RESET = esc
# enable map = m
# enable random obstacle avoiding wander = o
# enable obstacle avoiding go to (currently go to [0,0]) = p
# flat trim = t ## this seems incredibly dangerous
def control(drone, clock, screen, font):
    global oldtime, running, reached, autoEngage, enableMap, wander
    while running:
        for event in pygame.event.get():
        #bat = drone.navdata.get(0, dict()).get('battery', 0)
        #print("Battery: "),
        #print bat
            if event.type == pygame.QUIT:
                running = False
                changeRUNNING(running)
    #os.putenv("RUNNING", "FALSE")
                pygame.quit()
            elif event.type == pygame.KEYUP: # if a key is released, drone just hovers
                drone.hover()
            elif event.type == pygame.KEYDOWN: # key pressed down
                if event.key == pygame.K_ESCAPE:
                    print("EMERGENCY")
                    autoEngage = False
                    drone.reset()
                    running = False
                    changeRUNNING(running)
                    #os.putenv("RUNNING", "FALSE")
                elif event.key == pygame.K_RETURN:
                    print("Taking off...")
                    autoEngage = False
                    drone.takeoff()
                elif event.key == pygame.K_SPACE:
                    print("Landing...")
                    enableMap = False
                    autoEngage = False
                    drone.land()
                elif event.key == pygame.K_BACKSPACE:
                    print("Reset")
                    autoEngage = False
                    drone.reset()
                elif event.key == pygame.K_w:
                    drone.move_forward()
                elif event.key == pygame.K_s:
                    drone.move_backward()
                elif event.key == pygame.K_a:
                    drone.move_left()
                elif event.key == pygame.K_d:
                    drone.move_right()
                elif event.key == pygame.K_UP:
                    drone.move_up()
                elif event.key == pygame.K_DOWN:
                    drone.move_down()
                elif event.key == pygame.K_LEFT:
                    drone.speed = .5
                    drone.turn_left()
                    drone.speed = .2
                elif event.key == pygame.K_RIGHT:
                    drone.speed = .5
                    drone.turn_right()
                    drone.speed = .2
                elif event.key == pygame.K_p: # enable goto hardcoded point
                    drone.hover()
                    autoEngage = not autoEngage
                    goalReached = False
                    reached = False
                    print("Toggling autopilot = "),
                    print autoEngage
                elif event.key == pygame.K_m: # enable mapping
                    enableMap = not enableMap
                    print('enable feature map and EKF SLAM')
                elif event.key == pygame.K_o: # random wander
                    wander = not wander
                    print('enable obstacle reactive exploration')
                elif event.key == pygame.K_t: # send drone flat trim. very dangerous
                    drone.trim()
                    print("Sending trim")
                else:
                    print("Unrecognized input")

        clock.tick(50)

    print "Shutting down...",
    print drone.getPose()
    drone.pose.quit() # this makes the map save
    drone.halt()
    pygame.quit()
    print "OK."


def droneDisplay(drone, clock, screen, font):
    global xbs
    while running:
        surface = pygame.Surface((1280,800)) 
        hudbat = None
        hudpos = None
        hudsens = None
        pmap = None
        droneposeoverlay = None
        mapsurf = None
        droneoverlay = None
        
        try:
            hud_color = (255,10,10) if drone.navdata.get('drone_state', dict()).get('emergency_mask', 1) else (128,255,0)
            bat = drone.navdata.get(0, dict()).get('battery', 0)
            hudbat = font.render('Battery: %i%%' % bat, True, hud_color)
            pose = drone.getPose()
            rawx, rawy, rawyaw = drone.getRaw()
            hudpos = font.render('EKF Position: [%.2fX, %.2fY, %.2f0]' % (pose[0], pose[1], pose[3]), True, hud_color)
            hudpos2 = font.render('Raw Position: [%.2fX, %.2fY, %.2f0]' % (rawx, rawy, rawyaw), True, hud_color)
            hudsens = font.render('Sensors: [%.2f", %.2f", %.2f", %.2f"]' %(xbs[0], xbs[1], xbs[2], xbs[3]), True, hud_color)
            
            overlay = drone.getOverlay()           
            pmap = drone.getMap() # get prob map from drone. 800x800 array of uint8s
            pmap = pmap[...,None].repeat(3,-1).astype("uint8") # convert to 800x800x3 rgb array of uint8s
            mapsurf = pygame.surfarray.make_surface(pmap)
            droneoverlay = pygame.surfarray.make_surface(overlay)
            droneoverlay.set_colorkey((0,0,0))
        except Exception as e:
            print e
            hudbat = font.render('Battery: No connection to Drone', True, (30,30,30))
            hudpos = font.render('Positon: No connection to Drone', True, (30,30,30))
            hudpos2 = font.render('Positon: No connection to Drone', True, (30,30,30))
            hudsens = font.render('Sensors: No connection to XBee', True, (30,30,30))
        
        try:
            screen.blit(surface, (0,0))
            screen.blit(mapsurf, (480,0))
            screen.blit(droneoverlay, (480,0))
            screen.blit(hudbat, (10,10))
            screen.blit(hudpos, (10,40))
            screen.blit(hudpos2, (10,70))
            screen.blit(hudsens, (10,100))
        except Exception as e:
            print e
            pass
        pygame.display.flip() # this poorly named function is what actually pushes stuff to the screen
        clock.tick(50)
        pygame.display.set_caption("ARDrone-Nav || Mark Williams")

    
def droneAutopilot(drone, clock):
    global autoEngage, running, reached
    dest = [0,0]
    reached = False


##    while running:
##        if autoEngage:
#            if not reached:
#                current = drone.getPose()
#                xdif = dest[0] - current[0]
#                ydif = dest[1] - current[1]
#                desangle = math.atan2(ydif, xdif)
#
#                if abs(xdif) > .2 and abs(ydif) > .2:
#                    drone.speed = 1
#                    drone.move_forward()
               
    while running:
        if autoEngage:
            if not reached:    
                current = drone.getPose() #x, y, z, yaw in radians, time
                xdif = dest[0] - current[0]
                ydif = dest[1] - current[1]
                #print current
                #print ('%.2f, %.2f') % (xdif, ydif)
                desanglerad = math.atan2(ydif, xdif)
                desangle = desanglerad
                 
                if abs(xdif) > .2 and abs(ydif) > .2:
                    if True: # this line is for testing dead reckoning without sonar sensors enabled
                    #if ( xbs[0] > 10 and xbs[1] > 10 and xbs[2] > 10 and xbs[3] > 10 ): # if all sensors are greater than 10, try to go to point
                    #if abs(xdif) > .2 and abs(ydif) > .2:
                        angleDif = current[3] - desangle
                        if angleDif > .10: # radians
                            #print('Turn right!')
                            drone.speed = .5
                            drone.turn_right()
                            drone.speed = .2
                        elif angleDif < -.10: # radians
                            #print('Turn left!')
                            drone.speed = .5
                            drone.turn_left()
                            drone.speed = .2
                        else:
                            #print('Go forward!')
                            drone.move_forward()
                    else: # one of the sensors is too low, we have to do some obstacle avoidance
                        if xbs[0] < 10:
                            #print('Turn right to avoid obstacle!')
                            drone.speed = .5
                            drone.turn_right()
                            drone.speed = .2
                        elif xbs[3] < 10:
                            #print('Turn left to avoid obstacle!')
                            drone.speed = .5
                            drone.turn_left()
                            drone.speed = .2
                        elif xbs[1] < 10 or xbs[2] < 10: # front sensors are low, turn towards most open side
                            if xbs[0] < 10:
                                #print('Turn right to avoid front obstacle!')
                                drone.speed = .5
                                drone.turn_right()
                                drone.speed = .2
                            elif xbs[3] < 10:
                                #print('Turn left to avoid front obstacle!')
                                drone.speed = .5
                                drone.turn_left()
                                drone.speed = .2
                else:
                    print('Point reached!')
                    print current
                    drone.hover()
                    reached = True
                    autoEngage = False # turn autopilot off
        clock.tick(50)



def explore(drone, clock):
    global running, wander, STATE, NEXTSTATE

    frontobs = 30
    frontobsclear = 35
    sideobs = 25
    sideobsclear = 30
    HOVER = "HOVER"
    TURNLEFT = "TURNLEFT"
    TURNRIGHT = "TURNRIGHT"
    FORWARDS = "FORWARDS"
    REVERSE = "REVERSE"
    TURNLEFTSTART = "TURNLEFTSTART"
    TURNRIGHTSTART = "TURNRIGHTSTART"
    HOVERSTART = "HOVERSTART"
    HOVERFROMREVERSE = "HOVERFROMREVERSE"
    HOVERSTARTFROMREVERSE = "HOVERSTARTFROMNREVERSE"
    slow = .1
    fast = .2
    turnslow = .3
    turnfast = .5
    oldyaw = 0
    hovercounter = 0
    fobscounter = 0
    fobsclearcounter = 0
    while running:
        if wander:
            l = xbs[0]
            fl = xbs[1]
            fr = xbs[2]
            r = xbs[3]
            dp = drone.getPose()
            yaw = dp[3]

            if STATE == HOVERSTART:
                fobscounter = 0
                print("Hover")
                drone.hover()
                hovercounter = 0
                STATE = HOVER

            if STATE == HOVER:
                drone.hover()
                if hovercounter > 20:
                    if fl > frontobs and fr > frontobs:
                        STATE = FORWARDS
                    elif l > r:
                        STATE = TURNLEFTSTART
                    else:
                        STATE = TURNRIGHTSTART
                else:
                    hovercounter = hovercounter + 1
                    STATE = HOVER
            
            if STATE == HOVERSTARTFROMREVERSE:
                fobscounter = 0
                print("Hover after reverse")
                drone.hover()
                hovercounter = 0
                STATE = HOVERFROMREVERSE

            if STATE == HOVERFROMREVERSE:
                drone.hover()
                if hovercounter > 20:
                    if l > r:
                        STATE = TURNLEFTSTART
                    else:
                        STATE = TURNRIGHTSTART
                else:
                    hovercounter = hovercounter + 1
                    STATE = HOVERFROMREVERSE
                
            elif STATE == FORWARDS:
                drone.speed = slow
                drone.move_forward()
                if fl < frontobs or fr < frontobs:
                    #fobscounter = fobscounter + 1
                    #if fobscounter > 1:
                    STATE = REVERSE
                    #else:
                    #    STATE = FORWARDS
                elif l < sideobs or r < sideobs:
                    if l > r:
                        STATE = TURNLEFTSTART
                    else:
                        STATE = TURNRIGHTSTART
                else:
                    STATE = FORWARDS

            elif STATE == TURNLEFTSTART:
                print("Turn left")
                drone.speed = turnfast
                drone.turn_left()
                oldyaw = yaw
                STATE = TURNLEFT
                 
            elif STATE == TURNLEFT:
                drone.speed = turnfast
                drone.turn_left()
                if r > sideobsclear:
                    print abs(yaw - oldyaw)
                    if abs(yaw - oldyaw) > math.radians(20):
                        STATE = HOVERSTART
                    else:
                        STATE = TURNLEFT
                else:
                    STATE = TURNLEFT
            
            elif STATE == TURNRIGHTSTART:
                print("Turn right")
                drone.speed = turnfast
                drone.turn_right()
                oldyaw = yaw
                STATE = TURNRIGHT

            elif STATE == TURNRIGHT:
                drone.speed = turnfast
                drone.turn_right()
                print abs(yaw - oldyaw)
                if l > sideobsclear:
                    if abs(yaw - oldyaw) > math.radians(20):
                        STATE = HOVERSTART
                    else:
                        STATE = TURNRIGHT
                else:
                    STATE = TURNRIGHT

            elif STATE == REVERSE:
                drone.speed = slow
                drone.move_backward()
                if fl > frontobsclear and fr > frontobsclear:
                    fobsclearcounter = fobsclearcounter+1
                    if fobsclearcounter > 2:
                        STATE = HOVERSTARTFROMREVERSE
                        fobsclearcounter = 0
                    else:
                        STATE = REVERSE
                else:
                    STATE = REVERSE

            else: # default case
                drone.hover()
                STATE = HOVER
            clock.tick(50)
        
# currently runs inside of printsensor, which gathers the xbee sonar sensor informatipon
# should this run seperate? Unknown if missing important sensor data during
# time consuming hough transform        
def ekfslam(drone, clock, slam, ekf, posest):
    global running, enableMap
    offsets = [ [-.250, 0, -90], [-.200, .250, -10], [.200, .250, 10], [.250, 0, 90] ]
    #dronePose = drone.getPos()
    #pose = [ dronePose[0], dronePose[1], dronePose[2], dronePose[3] ]
    firstRun = True
    while running:
        if enableMap:
            if firstRun == True: # copy the drone's current base to the ekf slam system
                firstRun = False
                dp = drone.getPose()
                xy0 = [dp[0], dp[1], dp[3]]
                ekf.initBase(dp[0], dp[1], dp[3])
            if xbeeSonar.timestamp != oldtime:
                dronePose = drone.getPose()
                pose = [ dronePose[0], dronePose[1], dronePose[2], dronePose[3] ]
                slam.addReading(pose, offsets, xbs, ekf, posest)

        
        
def main():
    global oldtime
    global autoEngage
    global running
    #os.environ.putenv("RUNNING", "TRUE")  #environment variable for subprocesses
    #print os.environ.getenv("RUNNING") 
#   ## sensor stuff
#   thread.start_new_thread(xbeetest.read_sensor, () )
    changeRUNNING(True)
    running = True
    drone, clock, screen, font, posest = initDrone()
    slam = sonarSlam.sonarSlam()
    ekf = matrixSlam.matrixSlam()
    try:
        senseThread = Thread( target=xbeeSonar.read_sensor, args=(running, ) )
        senseThread.daemon = True
        senseThread.start()
        oldtime = xbeeSonar.timestamp
        
        printSensorT = Thread( target=printSensor, args=(drone, slam) )
        printSensorT.daemon = True
        printSensorT.start()

        autoPilotThread = Thread( target=droneAutopilot, args=(drone, clock) )
        autoPilotThread.daemon = True
        autoPilotThread.start()
        
        displayThread = Thread( target=droneDisplay, args=(drone, clock, screen, font) )
        displayThread.daemon = True
        displayThread.start()
            
        controlThread = Thread( target=control, args=(drone, clock, screen, font))
        controlThread.daemon = True
        controlThread.start()

        slamThread = Thread( target=ekfslam, args=(drone,clock,slam,ekf,posest))
        slamThread.daemon = True
        slamThread.start()

        wanderThread = Thread( target = explore, args=(drone, clock))
        wanderThread.daemon = True
        wanderThread.start()
        #controlThread = Thread( target=control, args=(drone, clock, screen) )
        #autoPilotThread = Thread( target=droneAutopilot, args=(drone, ) )
        #controlThread.daemon = True
        #autoPilotThread.daemon = True
        #controlThread.start()
        #autoPilotThread.start()
        
#   thread.start_new_thread(printSensor, () )   

    ## drone driving stuff
#   drone, clock, screen  = initDrone()
#   thread.start_new_thread( control, (drone, clock, screen))
#   thread.start_new_thread( droneAutopilot, (drone, ) )

        #while True:
        #   print("...Main loop checking in...")
        #   sleep(1)
    except KeyboardInterrupt as e:
        drone.land()
        


if __name__ == "__main__":
    main()
